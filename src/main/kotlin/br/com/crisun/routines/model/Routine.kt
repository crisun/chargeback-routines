package br.com.crisun.routines.model

import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Positive

class Routine(val id: Long, interval_in_seconds: Long, val command: Command, mensagem: String, var executionHistory: List<ExecutionHistory> = listOf()) {
    val interval_in_seconds = interval_in_seconds
        @Positive get() = field
    val mensagem = mensagem
        @NotEmpty get() = field
}