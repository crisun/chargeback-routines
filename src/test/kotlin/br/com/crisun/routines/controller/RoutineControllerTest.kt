package br.com.crisun.routines.controller

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@AutoConfigureMockMvc
@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RoutineControllerTest {
    @Autowired
    lateinit var mockMvc: MockMvc

    @Test
    fun `Insere uma rotina`() {
        mockMvc.perform(
            post("/routines")
                .contentType(MediaType.APPLICATION_JSON).content("{\"interval_in_seconds\":1,\"command\":\"write_to_file_one\",\"mensagem\":\"mensagem 3\"}")
        ).andDo(print()).andExpect(status().is2xxSuccessful)
    }

    @Test
    fun `Remove uma rotina`() {
        mockMvc.perform(
            delete("/routine/1")
        ).andDo(print()).andExpect(status().is2xxSuccessful)
    }

    @Test
    fun `Consulta todas as rotinas`() {
        mockMvc.perform(get("/routines")).andDo(print())
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.[0].id").value(1))
            .andExpect(jsonPath("$.[1].id").value(2))
    }

    @Test
    fun `Consulta uma rotina especifica`() {
        mockMvc.perform(get("/routine/1")).andDo(print())
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.id").value(1))
            .andExpect(jsonPath("$.executionHistory[0].id").value(1))
    }

    @Test
    fun `Remove uma rotina que nao existe`() {
        mockMvc.perform(
            delete("/routine/100")
        ).andDo(print()).andExpect(status().is2xxSuccessful)
    }

    @Test
    fun `Consulta uma rotina que nao exixte`() {
        mockMvc.perform(get("/routine/100")).andDo(print())
            .andExpect(status().is4xxClientError)
            .andExpect(content().string("Rotina nao encontrada"))
    }

    @Test
    fun `Tentativa de inserir uma rotina sem mensagem`() {
        mockMvc.perform(
            post("/routines")
                .contentType(MediaType.APPLICATION_JSON).content("{\"interval_in_seconds\":1,\"command\":\"write_to_file_one\",\"mensagem\":\"\"}")
        ).andDo(print()).andExpect(status().is4xxClientError)
    }

    @Test
    fun `Tentativa de inserir uma rotina com comando invalido`() {
        mockMvc.perform(
            post("/routines")
                .contentType(MediaType.APPLICATION_JSON).content("{\"interval_in_seconds\":1,\"command\":\"write_to_file_three\",\"mensagem\":\"mensagem 3\"}")
        ).andDo(print()).andExpect(status().is4xxClientError)
    }
}