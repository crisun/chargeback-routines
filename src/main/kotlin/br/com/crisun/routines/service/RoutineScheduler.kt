package br.com.crisun.routines.service

import br.com.crisun.routines.configuration.logger
import br.com.crisun.routines.model.Command
import br.com.crisun.routines.repository.FileWriterRepository
import br.com.crisun.routines.repository.RoutineRepository
import java.time.LocalDateTime
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit
import javax.annotation.PostConstruct
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler
import org.springframework.scheduling.support.PeriodicTrigger
import org.springframework.stereotype.Component

@Component
class RoutineScheduler(val routineRepository: RoutineRepository, val fileWriterRepository: FileWriterRepository) {
    val tasks: MutableMap<Long, ScheduledFuture<*>?> = mutableMapOf()
    val taskScheduler = ThreadPoolTaskScheduler().apply { initialize() }

    @PostConstruct
    fun start() {
        val routines = routineRepository.findAll()

        routines.forEach {
            addTask(it.id, it.interval_in_seconds, it.command, it.mensagem)
        }
    }

    fun removeTask(id: Long) {
        logger().info("Removendo a rotina: $id")
        tasks.remove(id)?.cancel(true)
    }

    fun addTask(id: Long, period: Long, command: Command, mensagem: String) {
        logger().info("Iniciando a rotina $id: $command, mensagem: $mensagem")

        val task = when (command) {
            Command.write_to_file_one -> Runnable {
                fileWriterRepository.writeToFileOne(mensagem)
                routineRepository.insertHistory(id, LocalDateTime.now())
            }
            Command.write_to_file_two -> Runnable {
                fileWriterRepository.writeToFileTwo(mensagem)
                routineRepository.insertHistory(id, LocalDateTime.now())
            }
        }

        tasks[id] = taskScheduler.schedule(task, PeriodicTrigger(period, TimeUnit.SECONDS))
    }
}