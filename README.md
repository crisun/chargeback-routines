# Routines
> Executa rotinas periodicamente.
Expõe uma api rest para cadastramento de rotinas que executam periodicamente. As rotinas executam dois comandos básico:
**write_to_file_one** - Escreve a mensagem cadastrada no arquivo *one.txt*
**write_to_file_two** - Escreve a mensagem cadastrada no arquivo *two.txt*
## Compilação

Compilando o projeto
```sh
./gradlew clean && ./gradlew build
```

Compilando o projeto e gerando uma imagem docker
```sh
./gradlew clean && ./gradlew build && docker build --no-cache -t crisun/routines .
```

Compilando o projeto e gerando uma imagem docker com o docker-compose
```sh
./gradlew clean && ./gradlew build && docker-compose build
```

## Inicialização

Utilizando docker-compose:
```sh
docker-compose up
```

Utilizando docker:
```sh
docker run --rm -it  -v /tmp:/app/data -p 8080:8080 crisun/routines
```

Executando manualmente:
```sh
java -jar routines-1.0.jar
```

## Exemplo de uso

Depois da aplicação inicializada temos a seguintes operações:

Consultar rotinas:
```sh
curl -H "Content-type: application/json" "http://localhost:8080/routines"
```

Consultar uma rotina:
```sh
curl -H "Content-type: application/json" "http://localhost:8080/routine/1"
```
Incluir uma rotina:
```sh
curl -v -XPOST -H "Content-type: application/json" "http://localhost:8080/routines" -d '{"interval_in_seconds":1,"command":"write_to_file_one","mensagem":"rotina de teste 1"}'
```
Excluir uma rotina:
```sh
curl -v -XDELETE -H "Content-type: application/json" "http://localhost:8080/routine/1"
```
*Nota:
Por default os arquivos são gerados em **/tmp***

## Biliotecas utilizadas
flywaydb: https://flywaydb.org

h2database: https://www.h2database.com

HikariCP: https://brettwooldridge.github.io/HikariCP