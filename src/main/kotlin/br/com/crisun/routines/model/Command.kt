package br.com.crisun.routines.model

enum class Command {
    write_to_file_one, write_to_file_two
}